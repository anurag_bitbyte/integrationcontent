import com.sap.gateway.ip.core.customdev.util.Message;
import java.util.HashMap;
import groovy.json.*
def Message processData(Message message) {
	
	 def body = message.getBody(java.lang.String) as String;
	 
	 
    def messageLog = messageLogFactory.getMessageLog(message);
   

     	def jsonSlurper = new JsonSlurper();
   		def parsedPayload = jsonSlurper.parseText(body);
    	message.setHeader("start",parsedPayload.start);   
    	message.setHeader("rows",parsedPayload.rows); 
    	message.setHeader("streamid",parsedPayload.streamid); 
    	 
  
	return message;
}
