
import com.sap.gateway.ip.core.customdev.util.Message;
import java.util.HashMap;
import groovy.json.*

def Message processData(Message message) {
	
	 def body = message.getBody(java.lang.String) as String;
	 
	 
    def messageLog = messageLogFactory.getMessageLog(message);
   

     	def jsonSlurper = new JsonSlurper();
   		def parsedPayload = jsonSlurper.parseText(body);
    	message.setHeader("accToken","Bearer "+parsedPayload.access_token);   
    	message.setHeader("refToken",parsedPayload.refresh_token); 
    	 messageLog.setStringProperty("Refresh Token is ", parsedPayload.refresh_token);
    	 messageLog.setStringProperty("Access Token is ", parsedPayload.access_token);
    	 
  
	return message;
}

